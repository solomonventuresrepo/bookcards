import { db as _db } from './config/local.env.json';
import { connect, connection, mongoose } from 'mongoose';

connect( _db.uri, _db.options );
const db = connection;

db.on('error', console.error.bind(console, 'connection error: ') );
db.once('open', function() {
  console.log("Connected to DB");
  // interact with DB
});

let Schema = mongoose.Schema;
let personSchema = new Schema({
  names: {
    surname: String,
    middle: String,
    first: String,
    other: [String],
    required: true
  },
  dateOfBirth: Date,
  addresses: [
    {
      name: String,
      place: String, 
      street: String,
      city: String,
      county: String,
      zipcode: String,
      country: String
    }
  ],
  nationalid: {
    names: {
      surname: String,
      middle: String,
      first: String,
      other: [String]
    },
    number: String,
    dateOfBirth: Date,
    issuer: String,
    issueDate: Date,
    expiration: Date,
    placeOfIssue: String,
    addressName: String
  }
});

exports = db, personSchema;