var path         = require('path');
var logger       = require('morgan');
var express      = require('express');
var router       = express.Router();
var bodyParser   = require('body-parser');
var createError  = require('http-errors');
var cookieParser = require('cookie-parser');
// Read in the configuration
const config     = require('./config/local.env.json');

var app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// API Service
var mongoose = require('mongoose');
mongoose.connect(config.db.uri + '/' + config.db.name, {useNewUrlParser: true})
  .then( () => console.log('connection successful') )
  .catch( (err) => console.error(err) );
var Streams  = require('./models/StreamModel.js');
var ABISConfig = require('./models/ABISConfig.js');
var ServiceSettings = require('./models/ServiceSettings');

//create api route
app.use('/api', router); // api root
app.use('/api/streams', router); // streams collection

app.post('/api', function (req, res) {
  res.json({message: "API root!"});
})

app.get( '/api/streams', function(req, res) {
  Streams.find(function(err, streams) {
    if (err) {
      res.send(err);
    } else {
      res.json(streams);
      console.log('Streams found ' + JSON.stringify(streams));
    }
  })
});

app.post('/api/streams', function (req, res) {
  var stream = new Streams();
  stream.id = req.body.id;
  stream.user_name = req.body.user;
  stream.thumbnail_url = req.body.thumbnail;
  stream.url = req.body.url;
  stream.title = req.body.title;

  stream.save(function(err) {
    if (err) {
      res.send(err);
    }
    res.json({ message: 'Stream create. Check MongoExpress!'});
  });
});

function getServiceSettings( srvcName, res ) {
  ServiceSettings.find(
    {servicename: srvcName}, 
  function(err, settings) {
    if (err) {
      res.send(err);
    } else {
      res.json(settings);
      console.log(srvcName + ' settings recovered ' + JSON.stringify(settings));
    }
  })
}

function insertServiceSettings(srvcName, req, res) {
  let settings = new ServiceSettings();
  settings.servicename = srvcName;
  settings.configsettings = {};
  Object.keys(req.body).forEach(function(key) {
    console.log( "[" + key + "] => " + req.body[key]);
    settings.configsettings.set( key, req.body[key]);
  });
  settings.save(function(err) {
    if (err) {
      res.send(err);
      res.json({ message: 'There is an issue' + err });
    } else {
      res.json({ message: srvcName + ' config settings changed. Check MongoExpress!'});
    }
  });
}

function updateServiceSettings(srvcName, req, res) {
  ServiceSettings.find(
    {servicename: srvcName},
    function(err, settings) {
      let newData = req.body
      console.log(settings[0]);
      if ( settings.length > 0 ) {
        let oldData            = settings[0];
        let theInfo            = new ServiceSettings();
        theInfo.servicename    = oldData.servicename;
        theInfo.configsettings = oldData.configsettings;
        theInfo._id            = oldData._id;
        Object.keys(req.body).forEach(function(key) {
          console.log( "[" + key + "] => " + req.body[key]);
          theInfo.configsettings.set( key, req.body[key]);
        });
        ServiceSettings.updateOne(
          {servicename: srvcName},
          {$set: theInfo},
          {upsert: true},
          function(err) {
            if (err) {
              res.send(err);
              res.json({ message: 'There is an issue' + err });
            } else {
              res.json({ message: 'Config settings updated. Check MongoExpress!'});
            }
          });
      }
    }
  )

}

// API Inserts
app.post( '/api/config/BiometricService', function(req, res) {
  insertServiceSettings('BiometricService', req, res);
});
app.post( '/api/config/EnrollmentService', function(req, res) {
  insertServiceSettings('EnrollmentService', req, res);
});

// API Updates
app.put('/api/config/EnrollmentService', function(req, res) {
  updateServiceSettings('EnrollmentService', req, res);
});
app.put('/api/config/BiometricService', function(req, res) {
  updateServiceSettings('BiometricService', req, res);
});

// API Getters
app.get( '/api/config/BiometricService', function(req, res) {
  getServiceSettings('BiometricService', res);
});
app.get( '/api/config/EnrollmentService', function(req, res) {
  getServiceSettings('EnrollmentService', res);
});


// Normal app stuff
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', indexRouter);
//app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', { title: 'Error', message: err.message });
});

module.exports = app;