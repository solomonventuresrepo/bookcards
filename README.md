# [BookCards by Mike Mangialardi](https://medium.com/coding-artist/redux-from-scratch-chapter-3-implementing-with-react-baf74c0b44ae)


I'm following the [@medium article](https://medium.com/coding-artist/redux-from-scratch-chapter-3-implementing-with-react-baf74c0b44ae) about creating a React-Redux web application.
Gotta keep learning.

To this end, his article was written in 2017, so there are some updates that must be introduced.

## Install Dependencies
An addendum dependency required is the webpack client.
To install type: 
```
npm i --save webpack-cli
```

At this point our `package.json` file looks like:
```
{
  "name": "bookcards",
  "version": "1.0.0",
  "description": "A react redux app",
  "main": "index.js",
  "scripts": {
    "start": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Jive Turkey",
  "license": "ISC",
  "dependencies": {
    "react": "^16.9.0",
    "react-dom": "^16.9.0",
    "react-redux": "^7.1.1",
    "redux": "^4.0.4",
    "webpack-cli": "^3.3.7"
  }
}
```

## Installing devDependencies

```
npm i --save-dev babel-loader @babel/core @babel/preset-env @babel/preset-react webpack webpack-dev-server html-webpack-plugin webpack-cli
```

Our `package.json` file should now look like:
```
{
  "name": "bookcards",
  "version": "1.0.0",
  "description": "A react redux app",
  "main": "index.js",
  "scripts": {
    "start": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Jive Turkey",
  "license": "ISC",
  "dependencies": {
    "react": "^16.9.0",
    "react-dom": "^16.9.0",
    "react-redux": "^7.1.1",
    "redux": "^4.0.4",
    "webpack-cli": "^3.3.7"
  },
  "devDependencies": {
    "@babel/core": "^7.5.5",
    "@babel/preset-env": "^7.5.5",
    "@babel/preset-react": "^7.0.0",
    "babel-loader": "^8.0.6",
    "html-webpack-plugin": "^3.2.0",
    "webpack": "^4.39.2",
    "webpack-dev-server": "^3.8.0"
  }
}
```

## Configuring Webpack
The only change from the original article deals with the module-rules-use-options.
These options must change due to the libraries/packages installed that are different
from what was originally posted.

```
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      }
    ]
  },
  plugins: [HtmlWebpackPluginConfig]
}
```