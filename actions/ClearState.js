function ClearState() {
  const CLEAR_STATE = 'CLEAR_STATE'
  return {
    type: CLEAR_STATE,
    status: "clear"
  }
}

export default ClearState