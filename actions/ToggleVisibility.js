// define action
function ToggleVisibility(componentName, isVisible) {
  const TOGGLE_VISIBILITY = "TOGGLE_VISIBILITY";
  switch (componentName) {
    case "SectionA":
      var sectionA_visible = isVisible;
      var sectionB_visible = !isVisible;
      return {
        type: TOGGLE_VISIBILITY,
        sectionA_visible,
        sectionB_visible
      }
    default:
      return {
        type: TOGGLE_VISIBILITY,
        sectionA_visible
      }
  }
}

export default ToggleVisibility