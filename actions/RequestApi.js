// Import library for API comms
import axios from 'axios';

// Import Fetching
import FetchFailure from './FetchFailure';
import FetchRequest from './FetchRequest';
import FetchSuccess from './FetchSuccess';

const uriPathGetStreams = `http://localhost:3000/api/streams`;

function RequestApi() {
  return (dispatch) => {
    const config = {
      headers: { 'Client-ID': 'x1f7xmsw1hdp1fp3f670zn1jgdbubv' }
    };
    //axios.get('give me an error!')
    //axios.get(`https://api.twitch.tv/helix/videos/?game_id=21779`, config)
    axios.get(uriPathGetStreams)
      .then(response => {
        console.log('Received the response' + JSON.stringify(response));
        if (response.data != null ) {
          //const streams = response.data.data.map(function(vid) {
          const streams = response.data.map(function(vid) {
            return {id: vid.id, user: vid.user_name, title: vid.title, thumbnail: vid.thumbnail_url, url: vid.url};
          });
          console.log(streams);
          // dispatch FetchSuccess, order 2
          dispatch(FetchSuccess(streams))
        } else {
          console.log("That shit empty");
          dispatch(FetchSuccess([]))
        }
      })
      .catch(error => {
        console.log(JSON.stringify(error));
        // dispatch FetchFailure, order 3
        dispatch(FetchFailure(error));
      });

      // dispatch FetchRequest, order 1
      dispatch(FetchRequest())
  }

}

export default RequestApi