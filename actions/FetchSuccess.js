// define action within an action creator
function FetchSuccess(streams) {
  const FETCH_SUCCESS = 'FETCH_SUCCESS'
  console.log("There's been a fetch success, returning status as success!");
  //console.log(streams);
  return {
    type: FETCH_SUCCESS,
    status: "success",
    streams
  }
}

export default FetchSuccess