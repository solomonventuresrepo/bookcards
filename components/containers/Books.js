import React from 'react';
import BookCard from  '../presentationals/BookCard';
//import { getState } from 'redux';
import AddBook from '../../actions/AddBook';
import DeleteBook from '../../actions/DeleteBook';
import EditBook from '../../actions/EditBook';
//Provider/Container React Component
class Books extends React.Component {

  componentDidMount() {
    this.props.store.subscribe(this.forceUpdate.bind(this));
  }

  dispatchAction(input) {
    switch (input) {
      case "TRASH":
        console.log("Should be trashing a book");
        this.props.store.dispatch(DeleteBook());
        break;
      case "PLUS":
        console.log("Should be adding a book");
        this.props.store.dispatch(AddBook());
        break;
      case "PENCIL":
        console.log("Should be editing a book");
        this.props.store.dispatch(EditBook());
        break;
    }
  }

  render() {
    const stateProps = this.props.store.getState();
    console.log(stateProps);
    const bookItems = stateProps.books.map( (book) => 
      <BookCard key = { book } 
                stateProps = { stateProps }
                dispatchAction = { this.dispatchAction.bind(this) } 
      />
    );
    return (
      <div className="books-container">
        {bookItems}
      </div>
    )
  }
}

export default Books