import React from 'react';
//import axios from 'axios';
import { getState } from 'redux';
import Alert from '../presentationals/Alert';
import Loader from '../presentationals/Loader';
import StreamCard from '../presentationals/StreamCard';
import TestCard from '../presentationals/TestCard';
import RequestApi from '../../actions/RequestApi';
//import ClearState from '../../actions/ClearState';

// Provider/Container react component
class Streams extends React.Component {

  componentDidMount() {
    this.props.store.subscribe(this.forceUpdate.bind(this));
    this.props.store.dispatch(RequestApi());
  }

  componentWillUnmount() {
    // this.props.store.dispatch(ClearState());
  }

  render() {
    const stateProps = this.props.store.getState();
    console.log( "Stream.js\n=========================\n" );
    console.log( JSON.stringify(stateProps.streams) );
    const streamItems = stateProps.streams.map( (stream) => 
      <TestCard
        key = { stream.id }
        streamLink = { stream.url }
        streamCover = { stream.thumbnail }
        streamTitle = { stream.title }
        user = { stream.user }
        id = {stream.id}
      />
    );
    return (
      <div>
        {stateProps.status === "loading" ? (
          <Loader />
        ) : (
          stateProps.status === "success" ? (
            <div className="tiles">
              {streamItems}
            </div>
          ) : (
            stateProps.status === "error" ? (
              <div>
                <Alert error = { stateProps.error } />
              </div>
            ) : (
            <div></div> 
            )
          )
        )
        }
      </div>
    )

  }
  /*
            <div className="tiles">
  const streamCardItems = stateProps.streams.map( (stream) =>
    <StreamCard
      user = { stream.user }
      key = { stream.id }
      streamTitle = { stream.title }
      streamCover = { String(stream.thumbnail).replace('%{width}','240').replace('%{height}','160') }
      streamLink = { stream.url }
    />
      <TestCard
        key = { stream.id }
        streamLink = { stream.url }
        streamCover = { stream.thumbnail }
        streamTitle = { stream.title }
        user = { stream.user }
      />
  );
  */
}

export default Streams