import React from 'react';

// Presentational React Component
class StreamCard extends React.Component {
  render() {
    return (
        <article className="style1">
          <div className="stream-cards">
            <a href={this.props.streamLink}>
              <span className="image">
              <img 
                className="stream-cover"
                src={this.props.streamCover}
              />
              </span>
            </a>
          </div>
          <div className="stream-title">
            Video title: <br/>{this.props.streamTitle}<br/> Streamer: {this.props.user}
          </div>  
        </article>
    )
  }
}

export default StreamCard