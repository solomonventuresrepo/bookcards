import React from 'react';
import { useState } from 'react';

// Presentational react component
class TestCard extends React.Component {
  render() {

    const handleSubmit = function(e) {
      e.preventDefault();
      alert('Submit information');
    }
    const result = (
      <article className="style1">
        <form ref="form" onSubmit={handleSubmit}>
          <label>Name: </label> <input id="name"/>
          <button type="submit" onClick={handleSubmit}>Complete</button>
        </form>
        <span className="image">
          <img src={this.props.streamCover} alt=""/>
        </span>
      </article>
    )
    /*
    const result = (
      <article className="style1">
        <form ref="form" onSubmit={handleSubmit}>
          <label>Name: </label> <input id="name"/>
          <button type="submit" onClick={handleSubmit}>Selections complete</button>
        </form>
        <span className="image">
          <img src={this.props.streamCover} alt=""/>
        </span>
        <a href={this.props.streamLink}>
          <h2>
            {this.props.id} - {this.props.streamTitle}
          </h2>
          <div className="content">
            Author: {this.props.user}
          </div>
        </a>
      </article>
    )
    */
    return result
  }
}

export default TestCard