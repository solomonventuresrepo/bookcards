import React from 'react';
import SectionA from './neighborhoods/SectionA';
import SectionB from './neighborhoods/SectionB';

class LandingPage extends React.Component {
  componentDidMount() {
    this.props.store.subscribe(this.forceUpdate.bind(this));
  }
  render() {
    const propStore = this.props.store;
    return (
      <div className="landing-page">
        <SectionA store={ propStore }/>
        <br/>
        <SectionB store={ propStore }/>
      </div>
    )
  }
}

//module.exports = LandingPage;
export default LandingPage;