import React from 'react';
import ToggleVisibility from '../../../actions/ToggleVisibility';

class SectionA extends React.Component {
  componentDidMount() {
    this.props.store.subscribe(this.forceUpdate.bind(this));
  }

  render() {
    const theStore     = this.props.store;
    const stateProps   = theStore.getState();
    const handleSubmit = function (e) {
      e.preventDefault();
      theStore.dispatch(ToggleVisibility("SectionA", !stateProps.sectionA_visible));
      alert('I want to make things invisible');
    }
    return (
      <div>
        {stateProps.sectionA_visible ? (
          <form ref="form" onSubmit={handleSubmit}>
            <span>Hello, are you ready to configure your system.</span>
            <button type="submit">Bring up configurable elements</button>
          </form>
          ) : null
        }
      </div>
    )
  }
}

//module.exports = SectionA;
export default SectionA;