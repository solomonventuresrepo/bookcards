import React from 'react';
import Streams from '../../containers/Streams';
import CheckBox from './blocks/houses/CheckBox';

class SectionB extends React.Component {
  componentDidMount() {
    this.props.store.subscribe(this.forceUpdate.bind(this));
  }
  render() {
    const theStore   = this.props.store;
    const stateProps = theStore.getState();
    const result = (
      <div>
      {stateProps.sectionB_visible ? (
        <div>
          <h1>List Modalities</h1>
          <br/>
          Is a checkbox showing? <CheckBox/>
          <section className="tiles">
            <Streams store={ theStore }/>
          </section>
        </div>
        ) : null
      }
      </div>
    )
    return result
  }
}

export default SectionB