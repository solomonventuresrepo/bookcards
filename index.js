import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, createStore } from 'redux';
import BooksApp from './reducers/BooksApp';
import TwitchApp from './reducers/TwitchApp';
// Books was code written for an earlier learning exercise 
import Books from './components/containers/Books';
import { Provider } from 'react-redux';
import Streams from './components/containers/Streams';
import LandingPage from './components/villages/LandingPage';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { Container, Row, Col } from 'react-grid-system';
// Create your own configuration file 
const config = require('./config/local.env.json');
// Include these things in order to get errors in a browser.
// Login at sentry.io, Solomon, you used your TechnoBrain info for signup.
// -- for deployment we would have to pay for the platform
// -- for development using the platform is free
// import Raven from 'raven-js';
// import createRavenMiddleware from 'raven-for-redux';

/*
class App extends React.Component {
  render() {
    return (
      <div className="app">
        <h1>Hello React & Redux Project</h1>
        <Books store={ store }/>
        <Streams store={ twitchStore }/>
      </div>
      <div className="main">
        <div className="inner">
          <LandingPage store={ twitchStore }/>
          <h1>List of Twitch Videos</h1>
          <section className="tiles">
            <Streams store={ twitchStore }/>
          </section>
        </div>
      </div>
    )
  }
}
*/
class App extends React.Component {
  render() {
    return (
      <div className="main">
        <div className="inner">
          <LandingPage store={ twitchStore }/>
        </div>
      </div>
    )
  }
}

/*
class App extends React.Component {
  render() {
    return (
      <LandingPage/>
    )
  }
}
*/

// for logging errors online to sentry.io
/*
if ( config.useOnlineLogs ) {
  console.log( 'Using ' + config.logs.online.name 
             + ' online logging platform by way of ' 
             + config.logs.online.url );
  Raven.config(config.logs.online.url).install()
} else {
  console.log( 'Not Using ' + config.logs.online.name 
             + ' online logging platform by way of ' 
             + config.logs.online.url );
}
*/
//initialize store
//let store = createStore(BooksApp)
let twitchStore = createStore(TwitchApp, 
  applyMiddleware(  thunk, 
                    logger ) )//,
                    //createRavenMiddleware(Raven, {}) ) )
//console.log(store.getState());
console.log("My twitch store\n\t" + JSON.stringify(twitchStore.getState()))
//console.log(twitchStore.getState());

  //<Provider store = { store } twitchStore = { twitchStore } >
ReactDOM.render(
  <Provider store = { twitchStore } >
    <App/>
  </Provider>,
  document.getElementById('main')
)
