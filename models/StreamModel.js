var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var streamSchema = new Schema({
  id: String,
  user_name: String,
  title: String,
  thumbnail_url: String,
  url: String
});

var Streams = mongoose.model('Streams', streamSchema);

module.exports = Streams;