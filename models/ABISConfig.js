var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var abisSchema = new Schema({
  token: String,
  abisserveripaddress: String,
  abisserveradminport: Number,
  fingersmaximalrotation: Number,
  matchingthreshold: Number,
  identificationmatchscore: Number,
  verificationmatchscore: Number,
  fingersqualitythreshold: Number,
  fingersminimalminutiacount: Number,
  irisesmaximalrotation: Number,
  irisesqualitythreshold: Number,
  facequalitythreshold: Number
});

var ABISConfig = mongoose.model('ABISConfigurationSettings', abisSchema);
module.exports = ABISConfig;