var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var settings = new Schema({
  servicename: String,
  configsettings: {
    type: Map,
    of: String
  }
});

var ServiceSettings = mongoose.model('ServiceSettings', settings);
module.exports = ServiceSettings;