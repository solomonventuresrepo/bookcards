// define the initial state
const initialState = {
  status: "loading",
  streams: [],
  error: "",
  sectionA_visible: true,
  sectionB_visible: false,
  sectionC_visible: false
}

// define a reducer with an initialized state and logic to handle action
function TwitchApp(state = initialState, action) {
  switch(action.type) {
    case 'FETCH_REQUEST':
      const requested = Object.assign({}, state, {
        status: action.status
      })
      return requested
    case 'FETCH_SUCCESS':
      const successful = Object.assign({}, state, {
        status: action.status,
        streams: action.streams
      } )
      return successful
    case 'FETCH_FAILURE':
      const failed = Object.assign({}, state, {
        status: action.status,
        error: action.error
      })
      return failed
    case 'TOGGLE_VISIBILITY':
      const toggleIt = Object.assign({}, state, {
        sectionA_visible: action.sectionA_visible,
        sectionB_visible: action.sectionB_visible
      })
      return toggleIt
    default:
      return state
  }
}

export default TwitchApp